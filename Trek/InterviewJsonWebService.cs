﻿using System;
using Newtonsoft.Json;

namespace Trek
{
    public class InterviewJsonWebService : IInterviewJsonWebService
    {
        private readonly HttpClient _httpClient;

        public InterviewJsonWebService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<BikeApiModel>> GetBikes()
        {
            var responseString = await _httpClient.GetStringAsync("https://trekhiringassignments.blob.core.windows.net/interview/bikes.json");

            var bikes = JsonConvert.DeserializeObject<List<BikeApiModel>>(responseString);
            if (bikes != null)
            {
                return bikes;
            }
            throw new Exception("InterviewJsonWebService.GetBikes failed.");
        }
    }
}


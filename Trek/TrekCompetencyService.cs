﻿using System;
namespace Trek
{
    public class TrekCompetencyService : ITrekCompetencyService
    {
        private readonly IInterviewJsonWebService _interviewJsonWebService;
        private readonly IBikesProcessor _bikesProcessor;

        public TrekCompetencyService(IInterviewJsonWebService interviewJsonWebService, IBikesProcessor bikesProcessor)
        {
            _interviewJsonWebService = interviewJsonWebService;
            _bikesProcessor = bikesProcessor;
        }

        public async Task DisplayTopTwentyModelCombinationsWithAmountSold()
        {
            var bikes = await _interviewJsonWebService.GetBikes();
            await _bikesProcessor.DisplayTopModelCombinationsSoldWithCount(bikes, 20);
        }
    }
}


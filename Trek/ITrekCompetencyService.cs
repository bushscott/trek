﻿
namespace Trek
{
    public interface ITrekCompetencyService
    {
        Task DisplayTopTwentyModelCombinationsWithAmountSold();
    }
}
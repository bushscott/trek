﻿
namespace Trek
{
    public interface IInterviewJsonWebService
    {
        Task<List<BikeApiModel>> GetBikes();
    }
}
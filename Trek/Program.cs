﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Trek;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services.AddTransient<IBikesProcessor, BikesProcessor>()
                .AddTransient<ITrekCompetencyService, TrekCompetencyService>()
                .AddHttpClient<IInterviewJsonWebService, InterviewJsonWebService>())
    .Build();

await CalculateMostPopularModel(host.Services);

static async Task CalculateMostPopularModel(IServiceProvider services)
{
    using IServiceScope serviceScope = services.CreateScope();
    IServiceProvider provider = serviceScope.ServiceProvider;

    ITrekCompetencyService trekCompetencyService = provider.GetRequiredService<ITrekCompetencyService>();

    await trekCompetencyService.DisplayTopTwentyModelCombinationsWithAmountSold();
}
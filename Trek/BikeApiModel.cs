﻿using System;
using Newtonsoft.Json;

namespace Trek
{
    public class BikeApiModel
    {

        [JsonConstructor]
        public BikeApiModel(
            [JsonProperty("bikes")] List<string> bikes
        )
        {
            this.Bikes = bikes;
        }

        [JsonProperty("bikes")]
        public IReadOnlyList<string> Bikes { get; }

    }
}


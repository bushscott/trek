﻿using System;

namespace Trek
{
    public class BikesProcessor : IBikesProcessor
    {
        public async Task DisplayTopModelCombinationsSoldWithCount(List<BikeApiModel> bikeOrders, int topAmount)
        {

            var comboCount = new Dictionary<string, int>();

            foreach (var bikeOrder in bikeOrders)
            {
                var ids = bikeOrder.Bikes.Select(x => x).OrderBy(x => x);
                var rowId = String.Join(",", ids);
                var rowIdCount = ids.Count();

                var seen = false;

                var comboCountList = comboCount.ToList();
                int currentRowCount = 1;

                foreach (var kvp in comboCountList)
                {
                    var key = kvp.Key;
                    if (key == rowId)
                    {
                        seen = true;
                        currentRowCount++;
                        continue;
                    }

                    var keySplit = key.Split(',');
                    var keyIdCount = keySplit.Length;

                    if (ids.Where(x => keySplit.Contains(x)).Count() == keyIdCount)
                    {
                        comboCount[kvp.Key] = kvp.Value + 1;
                    }
                    else if (keySplit.Where(x => ids.Contains(x)).Count() == rowIdCount)
                    {
                        currentRowCount++;
                    }
                }

                if (!seen)
                {
                    comboCount.Add(rowId, currentRowCount);
                }
                else
                {
                    comboCount[rowId] = currentRowCount;
                }
            }

            foreach (var kvp in comboCount.OrderByDescending(x => x.Value).Take(topAmount))
            {
                Console.WriteLine(String.Format("{0}: {1}", kvp.Key, kvp.Value));
            }
        }
    }
}
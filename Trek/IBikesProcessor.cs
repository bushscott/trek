﻿using System;
namespace Trek
{
    public interface IBikesProcessor
    {
        Task DisplayTopModelCombinationsSoldWithCount(List<BikeApiModel> bikeOrders, int topAmount);
    }
}

